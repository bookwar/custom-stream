# FIXME:
# For every source in sources.yaml create a Pulp remote which tracks it
#
# Then create a local Pulp repo which is synced from that remote
#
# Create a config file which lists the repos and current repository versions in each of them

set -ex

setup_source () {

  NAME="$1"
  URL="$2"

  echo "Creating $NAME repo from remote $URL"
  
  pulp rpm remote create --name "${NAME}-remote" --policy on_demand --url "$URL"
  pulp rpm repository create --name "${NAME}" --remote "${NAME}-remote"
  pulp rpm repository sync --remote "${NAME}-remote" --name "${NAME}" --sync-policy mirror_content_only

  TASK_HREF=$(pulp rpm publication create \
            --repository "${NAME}" \
            2>&1 >/dev/null | awk '{print $4}')

  PUBLICATION_HREF=$(pulp show --href "${TASK_HREF}" | jq -r '.created_resources | first')
  echo "$NAME published via ${PUBLICATION_HREF}"

  pulp rpm distribution create --name "${NAME}" --base-path "${NAME}" --repository "${NAME}"
  
}

setup_source centos-stream-9 "https://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/"
setup_source epel-9 https://dl.fedoraproject.org/pub/epel/9/Everything/x86_64/
setup_source sandbox https://download.copr.fedorainfracloud.org/results/bookwar/sandbox/centos-stream+epel-next-9-x86_64/


#
# centos-stream-9 published via /pulp/api/v3/publications/rpm/rpm/ebfdbd7c-79cf-4c60-868b-3e8f1900efa2/
# epel-9 published via /pulp/api/v3/publications/rpm/rpm/fa05069e-0655-45a1-9dab-bbd6d716a5e1/
# sandbox published via /pulp/api/v3/publications/rpm/rpm/be522d0f-6659-4d53-8ee1-d1e963e66ea6/

# https://pulp-web-svc-platform-stream.apps.auto-prod.gi0n.p1.openshiftapps.com/pulp/content/centos-stream-9/
# https://pulp-web-svc-platform-stream.apps.auto-prod.gi0n.p1.openshiftapps.com/pulp/content/epel-9/
# https://pulp-web-svc-platform-stream.apps.auto-prod.gi0n.p1.openshiftapps.com/pulp/content/sandbox/
